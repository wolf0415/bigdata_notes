#!/usr/bin/env python3



import sys

temp_count = 0
temp_word_length = None

for line in sys.stdin:

    line = line.strip()

    word_length, count = line.split('\t', 1) # value in the received input

    try:
        count = int(count)
    except ValueError:
        continue

    if word_length == temp_word_length:
        temp_count += count
    else:
        if temp_word_length != None:
            print(f"{temp_word_length}\t{temp_count}")
        temp_word_length = word_length # replace the temp_word_length from None to a value
        temp_count = count 

print(f"{temp_word_length}\t{temp_count}")
