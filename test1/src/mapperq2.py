#!/usr/bin/env python3

import re
import sys

for line in sys.stdin:
    line_string = line.strip()
    processed_string = re.sub(r'[^a-zA-Z0-9\ ]', '', line_string).lower() 
    words = processed_string.split()
    for word in words:
        if word == 'a' or word =='the' or word=='an': # return the entries of 'a', 'an' or 'the'
            print(f"{word}\t1")