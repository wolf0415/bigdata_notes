#!/usr/bin/env python3

import sys

temp_count = 0
temp_word_length = None

for line in sys.stdin:

    line = line.strip()

    # assign the variable with the tab separating value respectively
    word_length, count = line.split('\t', 1)

    try:
        count = int(count)
    except ValueError:
        continue

    if word_length == temp_word_length: 
        temp_count += count # increment by 1 for each matched word length 
    else:
        if temp_word_length != None:
            print(f"{temp_word_length}\t{temp_count}")
        temp_word_length = word_length  # replace the temp_word_length from None to a value
        temp_count = count 

print(f"{temp_word_length}\t{temp_count}")
