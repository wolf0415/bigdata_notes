#!/usr/bin/env python3

import re
import sys

for line in sys.stdin:
    line_string = line.strip()
    processed_string = re.sub(r'[^a-zA-Z0-9\ ]', '', line_string).lower()

    words = processed_string.split()

    for num in words:
        print(f"{len(num)}\t1") # print the word length and the number 1