# Practical 1 && 2
sed -i 's/chaos/burgers/g' sepiol.txt # means that substitute chaos to burgers in the all occurance of the text file
diff sam.txt sepiol.txt # find the difference between sam.txt and sepiol.txt

hadoop fs -put /home/prac/prac2/input/* /user/prac/prac2/input
hpf -ls /user/prac/prac2
hpf fs -mkdir -p /user/prac/prac2

cat file | mapper.py

head -2 file01.txt | mapper.py

cat file01.txt | ../src/mappper.py | sort | ../src/reducer.py 
# able to copy from our machine to Hadoop, while not applicable copying from Hadoop to Hadoop
hpf -copyFromLocal file01.txt /user/prac/prac6/input

# Run MapReduce job with Hadoop Streaming Utility, need to pass four param 
-input # input data
-output # output data, the output file must not existing




-mapper # specify mapper program 
-reducer # specify reducer program
mapred streaming -input /user/prac/prac2/input* -output /user/prac2/prac2/output -mapper /home/prac/prac2/src/mappper.py -reducer prac2/src/reducer.py

hpf -ls <dir>
hpf -cat <dir> | sort 

** Part 4 - Copy the output from HDFS **
# retrieve file from HDFS to linux 
hpf -get /user/prac/prac2/output/part* /home/prac/prac2/output/



