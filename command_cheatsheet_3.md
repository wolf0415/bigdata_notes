# Practical 4

# Use of Apache Spark 

- The native language for Spark is Scala, but we’ll use Python. There’s a Python API for Spark called PySpark

```bash
kali@kali~$ pyspark
# to see what methods available for us to use
>>> dir() # to list out methods that we can use 
>>> dir(SparkContext) # show methods available in that module
['PACKAGE_EXTENSIONS', '__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__enter__', '__eq__', '__exit__', '__format__', '__ge__', '__getattribute__', '__getnewargs__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_active_spark_context', '_assert_on_driver', '_checkpointFile', '_dictToJavaMap', '_do_init', '_ensure_initialized', '_gateway', '_getJavaStorageLevel', '_initialize_context', '_jvm', '_lock', '_next_accum_id', '_python_includes', '_repr_html_', '_serialize_to_jvm', 'accumulator', 'addFile', 'addPyFile', 'applicationId', 'binaryFiles', 'binaryRecords', 'broadcast', 'cancelAllJobs', 'cancelJobGroup', 'defaultMinPartitions', 'defaultParallelism', 'dump_profiles', 'emptyRDD', 'getCheckpointDir', 'getConf', 'getLocalProperty', 'getOrCreate', 'hadoopFile', 'hadoopRDD', 'newAPIHadoopFile', 'newAPIHadoopRDD', 'parallelize', 'pickleFile', 'range', 'resources', 'runJob', 'sequenceFile', 'setCheckpointDir', 'setJobDescription', 'setJobGroup', 'setLocalProperty', 'setLogLevel', 'setSystemProperty', 'show_profiles', 'sparkUser', 'startTime', 'statusTracker', 'stop', 'textFile', 'uiWebUrl', 'union', 'version', 'wholeTextFiles']
```
## Try some commands
```bash
>>>  sc.applicationId
'local-1646884537423' 
# our Spark application is running in ‘local’ mode (Spark Standalone Manager).
# If it were to be using YARN as a resource manager, the string preceding the long list of numbers would say ‘application’ instead. 
Eg: 'application-<random_numbers>'
We can define which cluster type to use when we make a program. For now, we will stick with the simple-to-use Spark Standalone Manage

``` 
```javascript
// load data in Spark
>>> data = sc.textFile("/user/prac/prac4/input/users.dat")

// By default, textFile() method thinks that the path you are running are in HDFS. 
// If dat file are not present in the HDFS, we can spcify file:// 
>>> data = sc.textFile("file:///home/prac/prac4/input/users.dat")
>>> type(data)
\<class 'pyspark.rdd.RDD'\> 
// Resilient Distributed Dataset (RDD) => Spark’s fundamental data structure, based on partitioning and parallelising. 
// textFile() change it to RDD by default
```

## commands that we can use in RDD data structure 
```javascript
// collect() function suitable for showing the result of calculation
>>> data.collect() // print the whole data in a list 
...'6021::M::25::12::08876', '6022::M::25::17::57006', '6023::M::25::0::43213', '6024::M::25::12::53705', '6025::F::25::1::32607', '6026::M::35::6::11210', '6027::M::18::4::20742', '6028::M::18::4::94133', '6029::F::25::1::23185', '6030::M::25::17::32618', '6031::F::18::0::45123', '6032::M::45::7::55108', '6033::M::50::13::78232', '6034::M::25::14::94117', '6035::F::25::1::78734', '6036::F::25::15::32603', '6037::F::45::1::76006', '6038::F::56::1::14706', '6039::F::45::0::01060', '6040::M::25::6::11106']
>>> data.first() // sh the first line of the txt file
'1::F::1::10::48067' 
>>> data.take(n) // the first n lines/items of the txt file
where n == 3,
['1::F::1::10::48067', '2::M::56::16::70072', '3::M::25::15::55117']
>>> data.takeSample(withReplacement:bool,size_of_the_subset_to_be_sample:int,random_seed[optional]:int)
data.takeSample(False,5,3) // return a fixed-size sampled subset of the RDD
['2466::M::25::15::94303', '2034::F::45::2::01851', '1340::M::25::7::14302', '3743::F::35::7::89128', '1790::M::25::0::19152']
>>> data.count() // count the number of elements in an RDD
6040
// Keep in mind that our text file, loaded as an RDD, does not yet discriminate between the variables
// that we can easily interpret by looking at the file. Instead, each row is saved as a single element

>>> data2 = data.union(data) // return the union of two RDD data

// ost of the time we’ll want to use textFile() to parallelize (make RDD’s out of) our external data.
// For two reasons,
// 1) normally that’s just how we get data, given to us in some txt or dat file,
// but 
// 2) if we try to generate data internally, creating our own RDD, it requires that the whole thing fits in memory.

data3 = [1,2,3,4]
// is used to create an RDD from a list collection
// parallesize() => try to generate data internally, creating our own RDD, it requires that the whole thing fits in memory.
rdd = sc.parallelize(data3) // used for prototyping and testing

>>> rdd.collect()
[1, 2, 3, 4]
>>> rdd.getNumPartitions() // check how many partitions our new RDD has been divided into
1
>>> data.getNumPartitions()
2

data4 = range(100)
data5 = range(5,105)
rdd1 = sc.parallelize(data4)
rdd2 = sc.parallelize(data5)
// We can use the zip command to zip one RDD with another one. It returns a key value pair where the
// first key and value are the elements of each RDD, the second key and value are the second elements
// of each RDD etc. As you might have inferred, this requires that each RDD has the same number of
// partitions as well as the same number of elements in each partition. Normally these conditions will
// be achieved through some transformation like mapping one RDD onto another (which we’ll learn
// more about next practical), fortunately, we happened to make such a pair of RDD’s with the new
// data4 and data5.
>>> rdd3 = rdd1.zip(rdd2)
>>> rdd3.collect()

```











