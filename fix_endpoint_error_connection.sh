#!/bin/bash

sudo systemctl stop hadoop-yarn.service
sudo systemctl stop hadoop-dfs.service
hadoop namenode -format -y

sudo systemctl start hadoop-dfs.service
sudo systemctl start hadoop-yarn.service
