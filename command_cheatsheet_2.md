# Practical 3
# Pig 
```bash
pig # run Pig on HDFS with MapReduce execution
pig -x local # run Pig locally ; -x options means that execute locally
```
## In grunt> interactive shell
```javascript
lines = LOAD '/home/prac/prac3/input/test.txt' AS (line:chararray); # produce a number of lines of output and start grunt shell
words = FOREACH lines GENERATE FLATTEN (TOKENIZE(line,' ')) AS word;
grouped = GROUP words BY word;
wordcount = FOREACH grouped GENERATE group, COUNT(words);
DUMP wordcount;
```
* he word count problem in Pig, from local mode, from a script (batch mode)

### Run a ping script locally
```javascript 
grunt> exec /home/prac/prac3/src/wordcount.pig
$ pig -x local /home/prac/prac3/src/wordcount.pig

pig -x local /home/prac/prac3/src/wordcount.pig | sort
```
** The word count problem in Pig, from MapReduce mode
- In order to run Pig in MapReduce mode, you need to upload the **test.txt** file to HDFS:
```javascript
$ hpf -mkdir -p /user/prac/prac3/input
$ hpf -put /home/prac/prac3/input/test.txt /user/prac/prac3/input 
```

## Getting the hand of Pig Latin 
```javascript
// Try to load the txt file
team = LOAD '/home/prac/prac3/input/team.txt' USING PigStorage(',') AS (firstName:chararray,lastName:chararray,position:chararray,playerNum:int,hometown:chararray,age:int);
$ DUMP
// Example output:
(Taylor,Walker,Fwd,13,Broken Hill,30)
(Rory,Sloane,Mid,9,Melbourne,30)
(Daniel,Talia,Def,12,Kilmore,28)
(Brad,Crouch,Mid,2,Ballarat,26)
(Matthew,Crouch,Mid,5,Ballarat,25)

$ DESCRIBE
// Example Output:
team: {firstName: chararray,lastName: chararray,position: chararray,playerNum: int,hometown: chararray,age: int};

$ EXPLAIN
#-----------------------------------------------
# New Logical Plan:
#-----------------------------------------------
team: (Name: LOStore Schema: firstName#61:chararray,lastName#62:chararray,position#63:chararray,playerNum#64:int,hometown#65:chararray,age#66:int)
|
|---team: (Name: LOForEach Schema: firstName#61:chararray,lastName#62:chararray,position#63:chararray,playerNum#64:int,hometown#65:chararray,age#66:int)
    |   |
    |   (Name: LOGenerate[false,false,false,false,false,false] Schema: firstName#61:chararray,lastName#62:chararray,position#63:chararray,playerNum#64:int,hometown#65:chararray,age#66:int)ColumnPrune:OutputUids=[64, 65, 66, 61, 62, 63]ColumnPrune:InputUids=[64, 65, 66, 61, 62, 63]
    |   |   |
    |   |   (Name: Cast Type: chararray Uid: 61)
    |   |   |
    |   |   |---firstName:(Name: Project Type: bytearray Uid: 61 Input: 0 Column: (*))
    |   |   |
    |   |   (Name: Cast Type: chararray Uid: 62)
    |   |   |
    |   |   |---lastName:(Name: Project Type: bytearray Uid: 62 Input: 1 Column: (*))
    |   |   |
    |   |   (Name: Cast Type: chararray Uid: 63)
    |   |   |
    |   |   |---position:(Name: Project Type: bytearray Uid: 63 Input: 2 Column: (*))
    |   |   |
    |   |   (Name: Cast Type: int Uid: 64)
    |   |   |
    |   |   |---playerNum:(Name: Project Type: bytearray Uid: 64 Input: 3 Column: (*))
    |   |   |
    |   |   (Name: Cast Type: chararray Uid: 65)
    |   |   |
    |   |   |---hometown:(Name: Project Type: bytearray Uid: 65 Input: 4 Column: (*))
    |   |   |
    |   |   (Name: Cast Type: int Uid: 66)
    |   |   |
    |   |   |---age:(Name: Project Type: bytearray Uid: 66 Input: 5 Column: (*))
    |   |
    |   |---(Name: LOInnerLoad[0] Schema: firstName#61:bytearray)
    |   |
    |   |---(Name: LOInnerLoad[1] Schema: lastName#62:bytearray)
    |   |
    |   |---(Name: LOInnerLoad[2] Schema: position#63:bytearray)
    |   |
    |   |---(Name: LOInnerLoad[3] Schema: playerNum#64:bytearray)
    |   |
    |   |---(Name: LOInnerLoad[4] Schema: hometown#65:bytearray)
    |   |
    |   |---(Name: LOInnerLoad[5] Schema: age#66:bytearray)
    |
    |---team: (Name: LOLoad Schema: firstName#61:bytearray,lastName#62:bytearray,position#63:bytearray,playerNum#64:bytearray,hometown#65:bytearray,age#66:bytearray)RequiredFields:null
#-----------------------------------------------
# Physical Plan:
#-----------------------------------------------
team: Store(fakefile:org.apache.pig.builtin.PigStorage) - scope-96
|
|---team: New For Each(false,false,false,false,false,false)[bag] - scope-95
    |   |
    |   Cast[chararray] - scope-78
    |   |
    |   |---Project[bytearray][0] - scope-77
    |   |
    |   Cast[chararray] - scope-81
    |   |
    |   |---Project[bytearray][1] - scope-80
    |   |
    |   Cast[chararray] - scope-84
    |   |
    |   |---Project[bytearray][2] - scope-83
    |   |
    |   Cast[int] - scope-87
    |   |
    |   |---Project[bytearray][3] - scope-86
    |   |
    |   Cast[chararray] - scope-90
    |   |
    |   |---Project[bytearray][4] - scope-89
    |   |
    |   Cast[int] - scope-93
    |   |
    |   |---Project[bytearray][5] - scope-92
    |
    |---team: Load(/home/prac/prac3/input/team.txt:PigStorage(',')) - scope-76

2022-03-04 15:44:12,638 [main] INFO  org.apache.pig.backend.hadoop.executionengine.mapReduceLayer.MRCompiler - File concatenation threshold: 100 optimistic? false
2022-03-04 15:44:12,640 [main] INFO  org.apache.pig.backend.hadoop.executionengine.mapReduceLayer.MultiQueryOptimizer - MR plan size before optimization: 1
2022-03-04 15:44:12,640 [main] INFO  org.apache.pig.backend.hadoop.executionengine.mapReduceLayer.MultiQueryOptimizer - MR plan size after optimization: 1
#--------------------------------------------------
# Map Reduce Plan                                  
#--------------------------------------------------
MapReduce node scope-97
Map Plan
team: Store(fakefile:org.apache.pig.builtin.PigStorage) - scope-96
|
|---team: New For Each(false,false,false,false,false,false)[bag] - scope-95
    |   |
    |   Cast[chararray] - scope-78
    |   |
    |   |---Project[bytearray][0] - scope-77
    |   |
    |   Cast[chararray] - scope-81
    |   |
    |   |---Project[bytearray][1] - scope-80
    |   |
    |   Cast[chararray] - scope-84
    |   |
    |   |---Project[bytearray][2] - scope-83
    |   |
    |   Cast[int] - scope-87
    |   |
    |   |---Project[bytearray][3] - scope-86
    |   |
    |   Cast[chararray] - scope-90
    |   |
    |   |---Project[bytearray][4] - scope-89
    |   |
    |   Cast[int] - scope-93
    |   |
    |   |---Project[bytearray][5] - scope-92
    |
    |---team: Load(/home/prac/prac3/input/team.txt:PigStorage(',')) - scope-76

$ ILLUSTRATE 
------------------------------------------------------------------------------------------------------------------------------------------
| team     | firstName:chararray   | lastName:chararray   | position:chararray   | playerNum:int    | hometown:chararray    | age:int    | 
------------------------------------------------------------------------------------------------------------------------------------------
|          | Rory                  | Sloane               | Mid                  | 9                | Melbourne             | 30         | 
------------------------------------------------------------------------------------------------------------------------------------------



// generate a relation for the first names of players 
first_name = FOREACH team GENERATE firstName;

```

* We can always use ***DUMP*** for error checking

```javascript
// generate a relatioin for the first name and last name of each player 
first_and_last_name = FOREACH team GENERATE firstName,$1;
```

* Use ***GROUP*** to group data by a certain field
```javascript
group_ages = GROUP team BY age; // Create a new group

grunt> ILLUSTRATE
---------------------------------------------------------------------------------------------------------------------------------------------------
| team     | firstName:chararray     | lastName:chararray     | position:chararray     | playerNum:int     | hometown:chararray     | age:int     | 
---------------------------------------------------------------------------------------------------------------------------------------------------
|          | Rory                    | Sloane                 | Mid                    | 9                 | Melbourne              | 30          | 
|          | Taylor                  | Walker                 | Fwd                    | 13                | Broken Hill            | 30          | 
---------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
| group_ages     | group:int     | team:bag{:tuple(firstName:chararray,lastName:chararray,position:chararray,playerNum:int,hometown:chararray,age:int)}                                 | 
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
|                | 30            | {(Rory, ..., 30), (Taylor, ..., 30)}                                                                                                                 | 
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

grunt> DESCRIBE
group_ages: {group: int,team: {(firstName: chararray,lastName: chararray,position: chararray,playerNum: int,hometown: chararray,age: int)}}
```

* Apply **AVG,MIN,MAX,SUM,COUNT**
```javascript
player_sums = FOREACH group_ages GENERATE SUM(team.playerNum);
group_hometown_and_position = GROUP team by (hometown,position);
```

* Use ***COGROUP*** operator to group two relations.
```javascript
power_team = LOAD '/home/prac/prac3/input/power.txt' USING PigStorage(',') AS (firstName:chararray,lastName:chararray,position:chararray,playerNum:int);
cogroup_data = COGROUP team BY position, power_team BY position; // group data based on some of the characteristics
```

## JOINs 
- self join, inner join and outer join 
```javascript
grunt> team2 = LOAD '/home/prac/prac3/input/team.txt' USING PigStorage(',') AS (firstName:chararray,lastName:chararray,position:chararray,playerNum:int,hometown:chararray,age:int);
grunt> joined_team = JOIN team BY playerNum, team2 BY playerNum;
grunt> crow_power = JOIN team BY age, poewr_team BY playerNum;
```
## Left outer join
```javascript
outer_left = JOIN team BY age LEFT OUTER, power_team BY playerNum;
```
## Right outer join
```javascript
outer_right = JOIN team BY age RIGHT OUTER, power_team BY playerNum;
```
## Full outer join
```javascript
outer_full = JOIN team BY age FULL OUTER, power_team BY playerNum;
```

# SPLIT 
```javascript
SPLIT team INTO team_young IF age<28, team_old IF age>27;
```
* Use **DUMP** to see the result 

## UNION
```javascript
original_team = UNION team_young, team_old;
```

## ORDER operator
```javascript
original_team_name_sort = ORDER orginal_team BY firstName;
```

## SAMPLE
* take a random sample from a relation. 
```javascript
sampled = SAMPLE tema 0.5;
```

## FILTER 
- use to select tuples from a relation based on a given condition
```
midfielders = FILTER team BY position == 'Mid';
```

## DISTINCT
```javascript
cleaned_team = DISTINCT team;
```
