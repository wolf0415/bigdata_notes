# Q1
# Create a list called data that has the numbers 1 to 5. Convert that list into an RDD called RDD1.
# Use the map function to create a new RDD called RDD2 that adds one to each element of RDD1. 
# Collect RDD2 to check your results

data = [i for i in range(1,6)]
rdd1 = sc.parallize(data)
rdd2 = rdd1.map(lambda x: (x+1))
rdd1.collect()


# Q2
# Create a new RDD called RDD3 that only contains the even numbers from RDD2. 
# Collect RDD3 to check your results

rdd3 = rdd2.filter(lambda x: x%2 == 0)
rdd3.collect()

# Q3
# Read the text file you created earlier and store it in an RDD called lines. 
# It’s a good idea to check whether the text document we’re working with has any empty lines and to remove them. 
# Use the count action to check how many rows are in the lines RDD.
# Now try using the appropriate transformation to keep only lines that aren’t empty

lines = sc.textfile("file:///home/prac/prac4/input/test_text.txt") # read the text file
lines.count() # count lines with empty lines included
lines_nonempty = lines.filter( lambda x: len(x) > 0) # remove any lines that are empty OR keep the lines that are not empty
lines_nonempty.count() # recount again 


# Q4 (Harder, uses a transformation we haven’t seen yet)
# It turns out, that rather than make intermediate RDD’s for every step, we can pipe a number of transformations together in the same command. 
# Recall that when we first discovered Pig one of the main draws was how much easier it was to write a word count program than it was in MapReduce?
# We wrote a script with a handful of commands and it achieved the same as the large Mapper and Reduce programs we used to write.
# In Spark, we can do it in one line:
counts = lines.flatMap(lambda line: line.split(" ")).map(lambda word: (word, 1)).reduceByKey(lambda v1, v2: v1 + v2)

# But what if we wanted to count the number of characters in the document instead? Can you do it in one line?
char_count = lines.map( lambda x: len (x)).reduce(lambda x, y: x+y)
