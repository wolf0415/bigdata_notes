lines = LOAD '/user/prac/prac3/input/test.txt' AS (line:chararray);
words = FOREACH lines GENERATE FLATTEN (TOKENIZE(line,' ')) AS word;
grouped = GROUP words BY word;
wordcount = FOREACH grouped GENERATE group, COUNT(words);
DUMP wordcount;

