# Practical 6
## Brief intro to Hive (Apache's data warehouse software)

* start **Hive**
```javascript
kali@kali~$ hive
```
### In hive, 
```sql
hive> SHOW TABLES;
/* as we haven't made any tables yet,*/
OK
Time taken: 0.699 seconds
```
* We can run a `hive` script with the script name `script.q`
```javascript
$ hive -f script.q
```
### Create table 
* Create a **empty** table called `users` with all the correct fields. 
```sql
hive> CREATE TABLE users (UsersID INT,
 n1 STRING, 
 Gender STRING, 
 n2 STRING, 
 Age INT, 
 n3 STRING, 
 OCCUPATION INT, 
 n4 STRING, 
 Zipcode STRING
 ) 
 ROW FORMAT DELIMITED FIELDS TERMINATED BY ':';
Output:
    OK
    Time taken: 1.128 seconds
```
### LOAD Data into `users` table 
* Load data into into the table
```sql
hive> LOAD DATA LOCAL INPATH '/home/prac/prac6/input/users.dat' OVERWRITE INTO TABLE users;
```

### DESC command
* `desc` is similiar to `describe` command from pig
```sql
hive> desc users; 
```

```sql
hive> SELECT * FROM users LIMIT 5;
hive> SELECT * FROM users WHERE  Gender == 'F' LIMIT 5;
hive> DROP TABLE users;
```

* In `LOAD` command we specified the term `LOCAL`, we are loading data from the **local OS**
* When we load the data from the local drive, the data is copied into HDFS. 
    * When we load data from HDFS, the data will moved from its current foldre. 
    * In this scenario, the you can find the file in the dir `/user/hive/warehouse`


* If you have already have your data file in HDFS and you don't want to move it to the Hive warehousre, it's not actually necessary to LOAD the file. You can create a table **connected** to the file instead
```sql
hive> CREATE EXTERNAL TABLE users (
    UserID INT,
    n1 STRING,
    Gender STRING,
    n2 STRING,
    Age INT,
    n3 STRING,
    Occupation INT,
    n4 STRING,
    Zipcode STRING
    ) ROW FORMAT DELIMITED FIELDS TERMINATED BY ':' LOCATION '/user/prac/prac6/input/';

```

* This is called an `EXTERNAL` table because the data are not inside the Hive warehouse. Please
note, that for this method we point not to the file but the location in HDFS, where the files are
stored.
* Similarly to the case with Python streaming, MapReduce will grab and process all files it
manages to find in the input folder. This feature is important if you get new files arriving into that
folder regularly, so you don’t care how many files are there – all of them will be connected to your
table.
* You just need to be certain that all of them share the same structure.
* If you plan to use several different datasets (like in this case where we have `users.dat`, `ratings.dat` and `movies.dat`), then each file should have its own folder. In which case we’d probably make a folder called users, a folder called ratings etc.
* Finally, we are ready to calculate the average age of males and females in the data. Like Pig, this is much easier to do in Hive than it is in plain MapReduce with Python streaming. The command is as
follows:

```sql
hive> SELECT Gender, AVG(Age) FROM users GROUP BY Gender;
```
* Given your experience with Pig, and potentially SQL, you should have a pretty good idea of what
this line is doing, but if not you can try to break it down piece by piece. This line will take a minute or so to run so don’t be alarmed while it’s running.
* Since this command involved data processing, Hive created java code for a MapReduce program
and executed it on the Hadoop system. Once again we’re drawn back to the same picture: whether
its Python streaming, Pig or Hive, all of the different applications create MapReduce programs and
run them on Hadoop. MapReduce is the only way we have to analyse Big Data.










