-- Q1
-- Load the abalone.data file
abalone_data = LOAD '/user/prac/test2/input/abalone.data' USING PigStorage(',') AS (abaloneID:chararray, sex:chararray, length:double, diameter:double,height:double, wholeWeight:double, shuckedWeight:double, visceraWeight:double, shellWeight:double, rings:int) ;

-- group abalone by sex
group_abalone = GROUP abalone_data BY sex ;

-- Count the number of each sex
abalone_count = FOREACH group_abalone GENERATE COUNT(abalone_data.sex) ;

-- Dump the count on screen 
DUMP abalone_count ;

-- Load the location.data file
location_data = LOAD '/user/prac/test2/input/location.data' USING PigStorage(',') AS (abaloneID:chararray, state:chararray) ;

-- Join the two relation data together
abalone_with_location = JOIN abalone_data BY abaloneID, location_data BY abaloneID ;


-- Q2b
-- group the abalone by state 
group_by_state = GROUP abalone_with_location BY state ;

-- Calculate the average shucked weight for each state
avg_weight = FOREACH group_by_state GENERATE AVG(abalone_with_location.shuckedWeight);

-- Dump the average shucked weight by state on screen
DUMP avg_weight ;


-- Q3
-- form a new relation that contains small abalone 
small_abalone = FILTER abalone_data BY wholeWeight < 0.5 ;

-- group relaton by all columns
group_small_abalone = GROUP small_abalone ALL ;

-- Calculate the average length of small abalone
small_avg_length = FOREACH group_small_abalone GENERATE AVG(small_abalone.length); 

-- Dump the average length for small abalone on screen
DUMP small_avg_length ;


-- form a new relation that contains big abalone 
large_abalone = FILTER abalone_data BY wholeWeight > 1.2 ;

-- group large_abalone with all columns 
group_large_abalone = GROUP large_abalone ALL ;

-- Calculate the average length of large abalone
large_avg_length = FOREACH group_large_abalone GENERATE AVG(large_abalone.length);

-- Dump the average length for large abalone on screen
DUMP large_avg_length ;

