from pyspark import SparkContext
from pyspark.sql import SparkSession

if __name__ == '__main__':

    # Definition of common variables
    filename = "file:///home/prac/test3/input/DataCoSupplyChainDataset.csv"
    file = open("/home/prac/test3/output/result.txt", 'w')

    # Define SparkContext and SparkSession
    sc = SparkContext(appName="Test3")
    spark = SparkSession(sc)

    # Q1 - Load data, convert to dataframe, apply appropriate column names and variable types
    data = sc.textFile("file:///home/prac/test3/input/DataCoSupplyChainDataset.csv").map(lambda x: x.split(","))
    df = data.toDF(["Type",\
                    "Customer_Country",\
                    "Customer_Fname",\
                    "Customer_Id",\
                    "Customer_Segment",\
                    "Order_Item_Product_Price",\
                    "Order_Item_Quantity",\
                    "Order_Item_Total",\
                    "Product_Name"\
                    ])
    df = df.withColumn("Order_Item_Product_Price", df["Order_Item_Product_Price"].cast("double"))\
        .withColumn("Order_Item_Quantity", df["Order_Item_Quantity"].cast("double"))\
        .withColumn("Order_Item_Total", df["Order_Item_Total"].cast("double"))

    # Q2 - Determine the proportion of each customer segment
    count_customer_segment = df.groupBy("Customer_Segment").count().collect()
    total_customer_segment = count_customer_segment[0][1] + count_customer_segment[1][1] + count_customer_segment[2][1]
    consumer_proportion = count_customer_segment[0][1] / total_customer_segment
    home_office_proportion = count_customer_segment[1][1] / total_customer_segment
    cooperate_proportion = count_customer_segment[2][1] / total_customer_segment

    file.write(f"Consumer = {consumer_proportion}%, Corporate = {cooperate_proportion}%, Home Office = {home_office_proportion}%\n\n")


    # Q3 - Which three products had the most sales
    # summarize the product's sales
    product_sales = df.groupBy("Product_Name").sum("Order_Item_Total").sort("sum(Order_Item_Total)", ascending=False)
    top_three_products = product_sales.take(3)
    file.write(f"{top_three_products[0][0]} = ${top_three_products[0][1]}, {top_three_products[1][0]} = ${top_three_products[1][1]}, {top_three_products[2][0]} = ${top_three_products[2][1]}\n\n")


    # Q4 - For each transaction type, determine the average item cost before discount
    # Prepare the columns needed to calculate average item cost 
    df_with_total_cost_column = df.withColumn("Total_Cost",df["Order_Item_Product_Price"]*df["Order_Item_Quantity"])

    col_needed_for_avg_cost_transaction = df_with_total_cost_column.groupBy("Type").sum("Order_Item_Product_Price","Order_Item_Quantity")
    # Transfer
    transfer = col_needed_for_avg_cost_transaction.filter(df.Type == "TRANSFER").collect()
    avg_transfer = transfer[0][1] / transfer[0][2] 

    # Cash
    cash = col_needed_for_avg_cost_transaction.filter(df.Type == "CASH").collect()
    avg_cash = cash[0][1] / cash[0][2]

    # Debit
    debit = col_needed_for_avg_cost_transaction.filter(df.Type == "DEBIT").collect()
    avg_debit = debit[0][1] / debit[0][2]

    # Payment
    payment = col_needed_for_avg_cost_transaction.filter(df.Type == "PAYMENT").collect()
    avg_payment = payment[0][1] / payment[0][2]

    file.write(f"Average item cost: Transfer = ${avg_transfer}, Debit = ${avg_debit}, Payment = ${avg_payment}, Cash = ${avg_cash}\n\n")


    # Q5 - What is the most regular customer first name in Puerto Rico
    # Count the number of customer in Puerto Rico
    puerto_rico_customer = df.filter(df.Customer_Country == 'Puerto Rico').groupBy("Customer_Id", "Customer_Fname").count()

    # Arrange in descending order to look for most regular ones 
    most_regular_customer_puerto_rico = puerto_rico_customer.sort("count", ascending=False).take(1)[0]
    name_of_most_regular_customer_puerto_rico = most_regular_customer_puerto_rico[1]
    num_of_visit_by_the_customer = most_regular_customer_puerto_rico[2]

    # Printing the solution to the results.txt file
    file.write(f"Most regular customer name in Puerto Rico is {name_of_most_regular_customer_puerto_rico}, who comes back {num_of_visit_by_the_customer} times.")

    file.close()
