# Practical 5
# Use of Spark Data structure -- DataFrame

## Ways to load data into Spark DataFrame 
### Use RDD as a medium (this work regardless of the Spark version)
```javascript
// load csv data into Spark RDD named WHData
WHData = sc.textFile("file:///home/prac/prac5/input/world-happiness-report.csv").map(lambda x: x.split(","))
// .toDF() => convert RDD into a DataFrame by providing a schema
// schema -> name of the column that you want to name with
>>> df = WHData.toDF(["CountryName","Year","LifeLadder","GDP","SocialSupport","LifeExpect","Freedom","Generosity","Corruption","Positive","Negative"])
// display data types of each column in the dataframe
>>> df.printSchema()
root
 |-- CountryName: string (nullable = true)
 |-- Year: string (nullable = true)
 |-- LifeLadder: string (nullable = true)
 |-- GDP: string (nullable = true)
 |-- SocialSupport: string (nullable = true)
 |-- LifeExpect: string (nullable = true)
 |-- Freedom: string (nullable = true)
 |-- Generosity: string (nullable = true)
 |-- Corruption: string (nullable = true)
 |-- Positive: string (nullable = true)
 |-- Negative: string (nullable = true)

// .cast() => change data type of the column to another
df.withColumn("<column name>","column".cast("<data type you want>"))
>>> df.withColumn("LifeLadder",df["LifeLadder"].cast("double"))
DataFrame[CountryName: string, Year: string, LifeLadder: double, GDP: string, SocialSupport: string, LifeExpect: string, Freedom: string, Generosity: string, Corruption: string, Positive: string, Negative: string]

>>> df.first() // at this stage, it doesn't parse the csv header (it regards the header as a data)
Row(CountryName='Country name', Year='year', LifeLadder='Life Ladder', GDP='Log GDP per capita', SocialSupport='Social support', LifeExpect='Healthy life expectancy at birth', Freedom='Freedom to make life choices', Generosity='Generosity', Corruption='Perceptions of corruption', Positive='Positive affect', Negative='Negative affect')
```

### Second MEthod: Use of csv() command in the pyspark.sql.Data.DataFrameReader()
```javascript
// By default, it will not parse the csv header 
>>> df2 = spark.read.csv("file:///home/prac/prac5/input/world-happiness-report.csv") 

//  csv() command reads the data into **DataFrame** columns "_c0" for the first column and "_c1" for
// the second and so on. And by default data type for all these columns is treated as String.
>>> df2.first()
Row(_c0='Country name', _c1='year', _c2='Life Ladder', _c3='Log GDP per capita', _c4='Social support', _c5='Healthy life expectancy at birth', _c6='Freedom to make life choices', _c7='Generosity', _c8='Perceptions of corruption', _c9='Positive affect', _c10='Negative affect')

// use option() before csv() to parse the header properly (treat the header as a header)
>>> df3 = spark.read.option("header",True).csv("file///home/prac/prac5/input/world-happiness-report.csv")

// After the option() used, the header have been recognised. The first() will show the first input value
>>> df3.first()
Row(Country name='Afghanistan', year='2008', Life Ladder='3.724', Log GDP per capita='7.370', Social support='0.451', Healthy life expectancy at birth='50.800', Freedom to make life choices='0.718', Generosity='0.168', Perceptions of corruption='0.882', Positive affect='0.518', Negative affect='0.258')
>>> df3.printSchema()
root
 |-- Country name: string (nullable = true)
 |-- year: string (nullable = true)
 |-- Life Ladder: string (nullable = true)
 |-- Log GDP per capita: string (nullable = true)
 |-- Social support: string (nullable = true)
 |-- Healthy life expectancy at birth: string (nullable = true)
 |-- Freedom to make life choices: string (nullable = true)
 |-- Generosity: string (nullable = true)
 |-- Perceptions of corruption: string (nullable = true)
 |-- Positive affect: string (nullable = true)
 |-- Negative affect: string (nullable = true
```

## Though process on problems that need to be tackled
### Determine how many countries in the dataset 
- To get a unique list elements from from an RDD, we can use `set()` 
- Looking through the list of transformations in the databricks pdf I find the distinct transformation, which **returns only the distinct elements of an RDD.**
```javascript
>>> distCountries = df3[0].distinct()
  File "<stdin>", line 1, in <module>
TypeError: 'Column' object is not callable
// distinct() needs an rdd as an input where df3[0] isn't. 
```
- To use `distinct()`, we must first convert the input into RDD
- use `.rdd` command and use `map()` to retrieve the first column 
- ***Standard way to convert dataFrame into RDD**
```javascript
>>> countriesRDD = df3.rdd.map(lambda x: x[0])
// Verify the result using the collect()
>>> countriesRDD.collect() // retrieve all the countries 
['Afghanistan', 'Afghanistan', 'Afghanistan', 'Afghanistan', 'Afghanistan', 'Afghanistan', 'Afghanistan', 'Afghanistan', 'Afghanistan', 'Afghanistan', 'Afghanistan', 'Afghanistan', 'Albania', 'Albania', 'Albania', 'Albania', 'Albania', 'Albania', 'Albania', 'Albania', 'Albania', ... ]
// Confirm we are on the right track through 
>>> countriesRDD
PythonRDD[31] at collect at <stdin>:1
>>> distCountries2 = countriesRDD.distinct()
>>> distCountries2.take(10)
['Afghanistan', 'Albania', 'Algeria', 'Angola', 'Argentina', 'Armenia', 'Australia', 'Austria', 'Azerbaijan', 'Bahrain']
>>> disCountries2.count() // get the number of unique countries in the dataset
```
### 2. Count how many times each country appears in the dataset
- Use `map()` to get key-values pair 
```javascript
// map each country to have a 1 next to it similiar to the first prac we did
>>> countryKVP = df3.rdd.map(lambda x: (x[0],1)) 
// Then we use the reduceByKey transformation.

// reduceByKey() -> to reduce the work string by applying the sum function on value. Count how occurance in total
>>> numPerCountry = countryKVP.reduceByKey(lambda v1, v2: v1+v2)
>>> numPerCountry.take(5)
[('Afghanistan', 12), ('Albania', 13), ('Algeria', 8), ('Angola', 4), ('Argentina', 15)]
```

### 3. Find the mean number of times a country appears, then create a new RDD that adds the mean to each count
```javascript
>>> numPerCountry.mean() // will fail
// This will yield an error stating ‘unsupported operand for -: 'tuple' and 'float'’
>>> justValues = numPerCountry.map(lambda x: x[1]) // RDD containing values only 
>>> justValues.first()
12
>>> justValues.mean()
Since we want to use this value and not just look at it, we can assign it to a variable.
>>> M = justValues.mean()
// It seems straightforward to add this value to the previous ones, lets use map to print the country
// name, then add M to the country count of our previous RDD.
>>> newRDD = numPerCountry.map(lambda x: (x[0], x[1]+M))
// Finally, what if we wanted to print some of these values alongside text using Python? Or manipulate
// them while printing? We’ll get an error stating that ‘PipelinedRDD’ object is not support
// subscriptable.
// We can’t pass an RDD into a print command, or do any elementwise operations on an RDD.
// Instead we’d have to collect the values, then the list elements can be accessed and manipulated as
// we normally would using square bracket notation.
>>> myList = newRDD.collect()
>>> print("Hello " + str(myList[0][0])
```

### 4. Find which country has the most life ladder values
- This could be done fairly easily using previous results, but lets try another approach.
- This sounds like we could group the data so that there’s a list of values for each country.
- The `groupByKey` transformation sounds appropriate. Let’s make a set of key-value pairs for country name and life ladder, then group that RDD by key.
```javascript
>>> ladderKVP = df3.rdd.map(lambda x: (x[0], x[2]))
>>> G = ladderKVP.groupByKey()
// This seems to work fine, but when we take a closer look at G using G.take(5) we see that alongside
// each country is not a list of numbers but an iterable object.
// In order to access their values for manipulation or aggregation we need to convert that object to a
// list.
>>> G2 = G.map(lambda x: (x[0], list(x[1])))
// Try taking the top 5 elements again and we should see something that aligns better with our
// expectations.
Next we want to get the length of the values in the list for each country.
>>> G3 = G2.map(lambda x: (x[0], len(x[1])))
// Finally, we can use the max action to get the largest value.
>>> G3.max()
// The result is Zimbabwe,15 but we already know that Canada also appears 15 times.
// It seems like max is finding the maximum letter value in the alphabet of the competing options.
// We can give the max action an optional function to indicate what data should be used to find the maximum value.
>>> G3.max(key=lambda x: x[1])
